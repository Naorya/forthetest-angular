import { environment } from './../environments/environment';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from "./products/products.service";
import { LoginComponent } from './login/login.component';
import { AddnamesComponent } from './products/addnames/addnames.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { ProductsfComponent } from './productsf/productsf.component';
import { SearchResultsComponent } from './products/search-results/search-results.component';







@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    NotFoundComponent,
    NavigationComponent,
    NotFoundComponent,
    ProductsComponent,
    LoginComponent,
    AddnamesComponent,
    EditProductComponent,
    ProductsfComponent,
    SearchResultsComponent,
    
  
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
       RouterModule.forRoot([
      {path:'',component:ProductsComponent},
      {path:'products', component:ProductsComponent},
      {path:'productsf', component:ProductsfComponent},
      {path:'app-search-results', component:SearchResultsComponent},
      {path:'login',component:LoginComponent},
      {path:'edit-product/:id', component:EditProductComponent},
      {path:'**',component:NotFoundComponent}//חשוב שהוא יהיה אחרון כי זה אומר - כל השאר... כל מה שלא הגדרנו ילך ל"לא נמצא"
      
    ])

  ],
  providers: [
     ProductsService,
     
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }