import { Component, OnInit } from '@angular/core';
import { ProductsService } from "../products/products.service";

@Component({
  selector: 'productsf',
  templateUrl: './productsf.component.html',
  styleUrls: ['./productsf.component.css']
})
export class ProductsfComponent implements OnInit {
products;
  constructor(private service:ProductsService) { }

  ngOnInit() {
      this.service.getProductsFire().subscribe(response=>
       {console.log(response);
          this.products= response;
     
        })
    }
  }
