import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsfComponent } from './productsf.component';

describe('ProductsfComponent', () => {
  let component: ProductsfComponent;
  let fixture: ComponentFixture<ProductsfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
