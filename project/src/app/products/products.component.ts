import { Component, OnInit } from '@angular/core';
import { ProductsService } from "./products.service";

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

    products;
  productsKeys =[];


  constructor(private service:ProductsService) {
    //let service = new UsersService();
    //this.spinnerService.show();
    service.getProducts().subscribe(response=>{
      //console.log(response.json());
      this.products = response.json();
      this.productsKeys = Object.keys(this.products);
      //this.spinnerService.hide();
    });
   }

  optimisticAdd(name){//user= $event <<------מהאי טי מל
    console.log("addName worked " + name);
    var newKey =  parseInt(this. productsKeys[this.productsKeys.length-1],0) +1;
    var newUserObject ={};//מגדיר שזה אובייקט כי זה מערך של אובייקטים
    newUserObject['name'] = name;
    this.products[newKey] = newUserObject;
    this.productsKeys = Object.keys(this.products);//לוקח את מסאג' קייז ומוסיף לו את האיבר שהוספנו במערך
      
  }
    deleteUser(key){
    console.log.apply(key);
    let index = this.productsKeys.indexOf(key);
    this.productsKeys.splice(index,1);
    //delete from server
    this.service.deleteProduct(key).subscribe(
      response=>console.log(response)
    );
  }
  
   
  pasemisticAdd(){
    this.service.getProducts().subscribe(response=>{
          console.log(response.json())//arrow function. .json() converts the string that we recieved to json
          this.products = response.json();
          this.productsKeys = Object.keys(this.products);
        
      });
        
    }  
  ngOnInit() {
  }

}