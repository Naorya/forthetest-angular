
import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ProductsService } from "../products.service";
@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

 
  @Output() addName:EventEmitter <any> = new EventEmitter <any>(); // -מגדירים את שם המשתנה בשם אווטפוט מסוג איונט אמיטר כאשר מבצעים השמה של אמיטר חדש ובכך בנינו תשתית של העברת מידע מהבן שהוא מסאג' פורם לאב שהוא מסאגס
  @Output() addNamePs:EventEmitter <any> = new EventEmitter <any>(); //pasemistic event

  service:ProductsService;
  addname = new FormGroup({//בנייה של מבנה נתונים בקוד שמתאים לטופס
      name:new FormControl(""),
      price:new FormControl(""),
      
  });

  sendData(){
    this.addName.emit(this.addname.value.name);

    console.log(this.addname.value);
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putProduct(this.addname.value, id).subscribe(
        response => {
          console.log(response.json());
          this.addNamePs.emit();
          this.router.navigateByUrl("/")//חזרה לדף הבית
        }
      );
    })
    
  }
  id;
  name;
  price;
  constructor(service:ProductsService, private formBuilder:FormBuilder, private route: ActivatedRoute, private router: Router) {   	    
     this.service = service;    
     this.route.paramMap.subscribe((params: ParamMap) => {
       this.id = +params.get('id'); // This line converts id from string into num      
       this.service.getProduct(this.id).subscribe(response=>{
        this.product = response.json();                                
        this.product = response.json();
        console.log(this.product.name);
        this.name = this.product.name
        this.price = this.product.price                                
       });      
     });
   }

   product;
   ngOnInit() {
     this.route.paramMap.subscribe(params=>{
       let id = params.get('id');
       console.log(id);
       this.service.getProduct(id).subscribe(response=>{
         this.product = response.json();
         console.log(this.product);
       })
   })
   }
   
   }