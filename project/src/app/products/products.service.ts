import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpParams } from '@angular/common/http';
import { Http,Headers } from '@angular/http';

import 'rxjs/Rx';
import { AngularFireDatabase } from "angularfire2/database";



@Injectable()
export class ProductsService {  login(arg0: any): any {
    throw new Error("Method not implemented.");
  }

http:Http;
  
getProductsFire(){//הפונקציה תתחבר לפייר בייס ותשמוך ממנה את הדאטא

  return this.db.list('/products').valueChanges();

  }
postProduct(data){//השיטה תקבל קובץ גייסון ותחליף אותו
    let options =  {
      headers:new Headers({//הגדרנו דרך מחלקה מיוחדת של אנגולר  שנקראת הדרס שליחה של קי וואליו
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('price',data.price);//פאראמס הוא למעשה מבנה נתונים שמחזיק קי ו- ואליו, קי הוא המאסג' והואליו הו א הדאטא.מסאג
     return this.http.post(environment.url+'products',params.toString(),options);
  }

deleteProduct (key){
    return this.http.delete(environment.url+'products/'+key)
  }
getProducts(){
    //return ['a','b','c'];
    //get users from the SLIM rest API (Don't say DB)
    return this.http.get(environment.url+'products');
  }
   putProduct(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.put(environment.url+'products/'+ key,params.toString(), options);
  }
   getProduct(id){
    
    let response =  this.http.get(environment.url+'products/'+id);
    return response;
  }
   constructor(http:Http,private db:AngularFireDatabase) { 
    this.http = http;
  }
}