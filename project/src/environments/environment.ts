// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyCS6Ll7xLqimg1xNlMmAc7rug3I-GBikbE",
    authDomain: "products-1ab96.firebaseapp.com",
    databaseURL: "https://products-1ab96.firebaseio.com",
    projectId: "products-1ab96",
    storageBucket: "products-1ab96.appspot.com",
    messagingSenderId: "831559656800"
  }
};